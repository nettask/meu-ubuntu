#!/bin/bash

NAME="Dir"
PERMISSAO="771"
PERM_DESC="rwxrwx--x"

# Gera um numero randomico
NUM1=$(echo $RANDOM)

# Aguarda 1s
sleep 1

# Gera outro numero randomico
NUM2=$(echo $RANDOM)

echo -e "ID1: $NUM1nID2: $NUM2nn"

# Verifica se a primeira variavel eh maior ou igual a segunda variavel
if [ $NUM1 -ge $NUM2 ];
then
echo -e "t$NUM1 eh maior que $NUM2"
else
echo -e "t$NUM1 eh menor que $NUM2"
fi

# Pegando o caminho do mkdir
MKDIR=$(which mkdir)

# Testo se mkdir possui permissao para executar no usuario atual, caso contrario
# || ira sair
test -x $MKDIR || exit 0

# Se chegou aqui eh porque passou no teste entao configuro o comando mkdir para
# /bin/mkdir -p -m 771, muito legal esse parametro de setar as permissoes na criacao
MAKE_DIR="$MKDIR -p -m $PERMISSAO"

# Chamo o MAKEDIR passo o NOME que dei na variavel NAME e junto o numero gerado
$MAKE_DIR $NAME$NUM1
$MAKE_DIR $NAME$NUM2

# Se o comando anterior executou sem problemas
# Usamos o $? para verificar
if [ $? -eq 0 ];
then
echo -e "n(*)Diretorios criado com sucesso!n"

# Listo os diretorios
ls -l | grep $NAME

# Mostro para o usuario as permissoes que foram setadas na criacao
echo -e "n(*)Permissoes: $PERMISSAO $PERM_DESC"
else
echo -e "n(*)Ocorreu um erro na criacao dos diretorios $NAME$NUM1 e $NAMENUM2!"
fi