#/bin/bash

#------------------------------------VARIAVEIS---------------------------------#

#--------------------------------------TESTES----------------------------------#
[ $UID -eq "0" ] || { echo "Necessita ser root..."; exit 1; } # É Root?

# Tem internet?
goo="http://www.google.com.br/intl/en_com/images/srpr/logo1w.png"
arq=$(basename "$goo")
wget -q $goo && [ -s "$arq" ] && rm "$arq" || { echo "Necessita de internet..."; exit 1; } 

#-------------------------------------FUNCOES----------------------------------#
function doInstalado() {
    app=$1 # aplivativo a ser verificado se esta instalado (1) ou não (0)
    [ $(which $app 2>/dev/null) ] && echo 1 || echo 0
}

function doSeparador() {
    echo ""
    echo "----------------------------"
    echo ""
}

function doExecutar() {
    exe=$1 # script a ser executado
    ati=$2 # flag de execucao 1 executa, 0 não executa
    app=$3 # aplicativo que é usado para a execucao do script
    if [ $ati -eq 1 ]; then
        [ "$(doInstalado $app)" -eq "1" ] && echo "Executando.: $exe ..." && sh -c "$exe" && doSeparador
    fi
}

function doInstalar() {
    exe=$1 # script de instalação do $exe
    ati=$2 # flag de execucao 1 executa, 0 não executa
    app=$3 # aplicativo a ser instalado
    api=$4 # aplicativo depois de instalado
    if [ $ati -eq 1 ]; then 
        # alimenta o nome do apl para a funcao que fará o which
        [ "$api" != "$app" ] && apl=$api || apl=$app
        if [ "$(doInstalado $apl)" -eq "1" ]; then
            echo "$app Já instalado" && doSeparador
        else
            sh -c "$exe" && doSeparador
        fi
    fi
}

function doListarUsuarios() {
    usu=""
    for i in $(cat /etc/passwd); do
        uid=$(echo $i | cut -d":" -f3) #id
        uno=$(echo $i | cut -d":" -f1) #nome
        [ $uid -ge "1000" 2>/dev/null ] && [ $uno != "nobody" ] && usu="$usu $uno"
    done
    echo "$usu"
}

function doArquitetura64() {
    # 1 amd64 0, i386
    [[ -n $(uname -a | grep "x86_64") ]] && echo "1" || echo "0"
}

#----------------------------------REPOSITORIOS--------------------------------#

#-----------------------------------APLICATIVOS--------------------------------#

#------------------------------------------------------------------------------#
doSeparador #inicio do script

# ----                         ---- #
# ---- INSTALAÇÃO COM APT-GET  ---- #
# ----                         ---- #

# 0 desligado, 1 ligado
executa=1

# ----
apps="ubuntu-restricted-extras"                                     #pacotes de aplicações restritas do ubuntu
apps="$apps build-essential g++ wget curl"                          #ferramentas base
apps="$apps python3-pyudev"                                         #dependência para o multibootusb
apps="$apps python-apt"                                             #para o steam
apps="$apps lolcat"                                                 #cores no shell, utilizado para fetch
apps="$apps figlet"                                                 #gera escritas via terminal
apps="$apps cmake"                                                  #compilador em C
apps="$apps libudev-dev"                                            #biblioteca compartilhada

# ---- redes                            
apps="$apps net-tools"                                              #(ifconfig entre outros)
apps="$apps iproute2 iproute2-doc"                                  #(comandos de rede tb)
apps="$apps iptraf"                                                 #(verificar trafego de rede)
apps="$apps nmap zenmap"                                            #(verificacao de status de rede)
apps="$apps whois"                                                  #identificacao de proprietario de dominio"
apps="$apps filezilla"                                              #ftp
apps="$apps wireshark"                                              #ferramenta de invasao
apps="$apps dnsutils"                                               #informacoes de dns
apps="$apps cntlm"                                                  #conecta em proxy de forma automatica

# ---- dev  
apps="$apps openjdk-14-jdk"                                         #jdk
apps="$apps git"                                                    #repositorio
apps="$apps dialog"                                                 #criacao de telas na linha de comando
apps="$apps shellcheck"                                             #verifica bugs em scripts shell
apps="$apps tomcat9 tomcat9-admin tomcat9-docs tomcat9-examples tomcat9-user" #tomcat
apps="$apps nodejs"                                                 #framework frontend
apps="$apps npm"                                                    #gerenciador de pacotes do node
apps="$apps composer"                                               #framework backend
apps="$apps dia"                                                    #editor de fluxogramas, UML, etc...
apps="$apps recode"                                                 #converte arquivos em diferentes tipos de formatos (UTF8 em ISO8859...) >> $ cat oie.txt | recode html.utf8
apps="$apps postgresql"                                             #postgresql
apps="$apps pgadmin3"                                               #ide para postgresql
apps="$apps apache2 apache2-utils"                                  #apache e Apache Bench >> $ ab -n 10 [url] >> envia 10 requisicoes para a url informada
apps="$apps mysql-server php-mysql"                                 #mysql
apps="$apps php"                                                    #php
#apps="$apps mysql-workbench"                                       #ide para mysql (não está no repo padráo)
#apps="$apps phpmyadmin"                                            #admin para mysql (usando no /home)

# ---- aplicativos
apps="$apps arc arj lhasa p7zip-rar rar unace bzip2 gzip unzip"     #compactadores
apps="$apps cups-pdf"                                               #impressora PDF
apps="$apps chromium-browser chromium-browser-l10n"                 #versao da comunidade do Chrome
apps="$apps vim"                                                    #vi melhorado
apps="$apps gparted"                                                #editor de partição
apps="$apps htop"                                                   #top melhorado
apps="$apps playonlinux"                                            #interface para wine
apps="$apps locate"                                                 #localizacao de arquivos no computador
apps="$apps calibre"                                                #gerenciador de ebook
apps="$apps dconf-editor"                                           #editor de configuração, util para identicar quais as chaves são usadas
apps="$apps terminator"                                             #terminal shell

# ---- videos e graficos                            
apps="$apps openshot"                                               #editor de videos
apps="$apps blender"                                                #editor 3D
apps="$apps vlc"                                                    #player
apps="$apps gimp"                                                   #editor de imagens

# ---- gnome                                
apps="$apps gnome-tweaks"                                           #ferramentas para gnome
apps="$apps gir1.2-gtop-2.0 gir1.2-nm-1.0 gir1.2-gconf"             #para extensões do gnome
apps="$apps chrome-gnome-shell"                                     #extensoes do shell no chrome
apps="$apps gnome-shell-extension-system-monitor"                   #base para extensao system monitor

# ---- nautilus                         
apps="$apps gnome-sushi"                                            #visualizador de arquivos (clique no arqivo e press espaço)
apps="$apps nautilus-admin"                                         #menu de admin no nautilus
apps="$apps nautilus-dropbox"                                       #dropbox

# ---- grub                         
#apps="$apps grub-customizer"                                       #ferramentas para grub (não está no repo padrão)

# ---- mobile                           
apps="$apps android-tools-adb"                                      #ferramentas para android
apps="$apps android-tools-fastboot"                                 #ferramentas para android

# ---- hardware 
apps="$apps inxi"                                                   #informações sobre o hardware
apps="$apps neofetch"                                               #informações sobre o hardware/software
apps="$apps lshw"                                                   #mostra informações do hardware (Ex.: para ver video digite # lshw -C video)

# ---- virtualização                                    
apps="$apps virtualbox"                                             #maquina virtual

# ---- aws                          
apps="$apps python-pip"                                             #gerenciador de pacotes python       
apps="$apps s3fs"                                                   #monta particao AWS s3
apps="$apps awscli"                                                 #cliente aws para linha de comando

# ---- games                                    
apps="$apps steam"                                                  #plataforma de games

# --- conky                                 
apps="$apps lua5.3 dos2unix"                                        #para o conky
apps="$apps conky-all"                                              #conky

# --- kodi                                 
apps="$apps kodi"                                                   #Player multimidia
apps="$apps kodi-pvr-iptvsimple"                                    #addon para iptv


[ $executa -eq 1 ] && apt update && apt dist-upgrade -y && apt install -y $apps && apt -y autoremove && doSeparador

# ----                      ---- #
# ---- INSTALAÇÃO COM SNAP  ---- #
# ----                      ---- #

# 0 desligado, 1 ligado
executa=1

if [ $executa -eq 1 ] 
then
    # ---- comunicacao
    snap install slack --classic                                    #compartilhamento de tarefas
    snap install skype --classic                                    #comunicador

    # ---- dev  
    #snap install android-studio --classic                          #ide para desenvolvimento mobile (coloquei no /home)
    #snap install code --classic                                    #ide para desenvolvimento da microsoft (coloquei no /home)
    #snap install eclipse --classic                                 #ide para desenvolvimento (coloquei no /home)

    doSeparador
fi

# ---- comunicacao                          
apps="discord"                                                      #rede social de games
apps="$apps whatsdesk"                                              #cliente whatsapp

# ---- aplicativos                                          
apps="$apps freemind"                                               #criação de mapas mentais
apps="$apps udemy"                                                  #EAD da Udemy
apps="$apps fast"                                                   #testa a velocidade da internet
apps="$apps darktable"                                              #processamento de fotos RAW
apps="$apps drawing"                                                #editor de imagens ao estilo paint

# ---- dev                          
#apps="$apps gitkraken"                                             #gerenciador de repositorio git (coloquei no /home)
apps="$apps angstrom"                                               #gerenciador de projetos
apps="$apps scratux"                                                #linguagem de programação infantil
apps="$apps grv"                                                    #interface via linha de comando para o GIT
apps="$apps electronplayer"                                         #Player baseado no Electron para Netflix, Youtube, Twitch, Floatplane, Hulu

[ $executa -eq 1 ] && snap refresh && snap install $apps 2>>/dev/null 

doSeparador

# ----                      ---- #
# ----  OUTRAS INSTALAÇÕES  ---- #
# ----                      ---- #

# 0 desligado, 1 ligado
executa=1

# ---- Extension Pack of Virtualbox
vs="6.0.12"
end="http://download.virtualbox.org/virtualbox"
arq="Oracle_VM_VirtualBox_Extension_Pack-$vs.vbox-extpack"
# ----
# não há necessidade de testar se o arquivo existe, pois o wget já o faz
# -c continuo, -P seta o diretorio onde será colocado o arquivo
wget -c -P "/tmp" "$end/$vs/$arq"
exec="virtualbox /tmp/$arq"
    doExecutar "$exec" $executa "virtualbox"

# ---- Google Chrome
prg="google-chrome"
end="https://dl.google.com/linux/direct"
a64="google-chrome-stable_current_amd64.deb"
a86="google-chrome-stable_current_i386.deb"
[ "$(doArquitetura64)" -eq "1" ] && arq="$a64" || arq="$a86"
# ----
wget -c -P "/tmp" "$end/$arq"
exec="dpkg -i /tmp/$arq"
    doInstalar "$exec" $executa "$prg" "$prg"

# ---- AWS Elastic Beanstalk cliente
prg="awsebcli"
# ----
exec="pip install $prg --upgrade --user"
    doInstalar "$exec" $executa "$prg" "eb"

#---------------------------------------EXTRAS---------------------------------#

# 0 desligado, 1 ligado
executa=1

# ---- updatedb
exec="updatedb" #atualiza base do locate
    doExecutar "$exec" $executa "locate"

# ---- vboxusers
for i in $(doListarUsuarios); do
    exec="adduser $i vboxusers"
        doExecutar "$exec" $executa "virtualbox"
done

# ---- coloca neofetch para inciar
arq="/etc/profile.d/mymotd.sh"
# ----
exec="echo "neofetch" > $arq && chmod +x $arq"
    doExecutar "$exec" $executa "neofetch"

#CONFIGURACAO DO MARIA DB
# ---- configuração do MariaDb
# >>>> inserir pergunta se deseja fazer a configuração <<<<<
#$info="S" && echo "Configurar MariaDB? [S/n]" && read $info
#[ ! $info == 
#exec="mysql_secure_installation" #configuracao do MariaDb

#echo "Executando $exec ..." 
#sh -c "$exec"

##############################ARQUIVOS DE CONFIGURAÇÃO##########################

#CONFIGURACAO NVIDIA
# comentar a linha "options nvidia-drm modeset=1"
# no arquivo /lib/modprobe.d/nvidia-kms.conf

#CONFIGURAÇÃO DO IPV4 
# 99force-ipv4 para apt
#echo 'Acquire::ForceIPv4 "true"' > /etc/apt/apt.conf.d/99force-ipv4

#CONFIGURAÇÃO PARA ALTERAR TEMPO DE ACESSO AO SSD E ABSTRAIR MSG DE ERRO
#/etc/default/grub alterar
# GRUB_CMDLINE_LINUX_DEFAULT="quiet" para GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pci=nomsi"

# CONFIGURACAO DO $PS1 PADRÃO

# colocar /snap/bin no PATH por conta do snap

# colocar /root/.local/bin  no PATH por conta do awsebcli

# colocar no terminator para executar como shell de login e cores verdes

# colocar logs e diminuir textos na tela

# colocar barra de progresso

# criar uma tela interativa (dialog ou pyton...)

################################## AVALIAÇAO ###################################

# teamviewer

# chob, pesquisa em AppImage, Flathub e SnapCraft
# wget https://github.com/MuhammedKpln/chob/releases/download/0.3.5/chob.deb
