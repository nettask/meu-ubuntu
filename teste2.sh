#!/bin/bash

#--------------------------------------------------------------------#
# INÍCIO >>> function remove_da_lista_de_tarefas()
#--------------------------------------------------------------------#
function remove_da_lista_de_tarefas()
{ 
# Definindo variáveis locais.
	local lc_prefix_file_erro lc_dir_arq_start
# Prefixo para usar como arquivo de erro.
	lc_prefix_file_erro="$1"
# Diretório para enviar o arquivo de start/início.
	lc_dir_arq_start="$2"
# Sufixo para comandos. Poderá ser passado via parâmetros no futuro.
	lc_sufix_comando=' cut -f1 -d":"'
# Variável que possui o comando completo para gerar o arquivo.
	lc_comando_completo="${lc_dir_arq_start}/${lc_prefix_file_erro} |
	${lc_sufix_comando}"
# Variável que orienta dados e o arquivo de erro.
	lc_corrigir_erro_trf="${dir_pasta_start}/Corrigir_Erro_${$}.trf"
# Endereço do arquivo temporário para determinada tarefa.
	lc_tmp_tarefa="${dir_pasta_servicos}/tmp_tarefa.$$"
# Endereço completo para o arquivo que irá receber os erros.
	lc_tmp_corrigir="${dir_pasta_servicos}/tmp_corrigir.$$"
# Linha de Debug (função) parametrizado para passar informações 
# ao sistema de depuração.
	Debug 4 "comandos [ eval cat  completo ${lc_comando_completo} >
	${lc_corrigir_erro_trf} ] Função:$FUNCNAME Linha:$LINENO]"
# Linha de comando para gerar o arquivo. Fiz com eval porque não estava
# funcionando de outra forma. Detesto usar eval.
	eval cat "${lc_comando_completo}" >
	${lc_corrigir_erro_trf} 2> /dev/null
# Testa se o arquivo de erro foi encontrado.
	if [ -f ${lc_corrigir_erro_trf} ]
	then
           StatusErroTarefa="1" 
# Linha de debug para acompanhamento
		MSG="Entrou no se existe arquivo: ${lc_corrigir_erro_trf}"
		Debug 1 "$MSG  [Função:$FUNCNAME Linha:$LINENO]"
# Vai rodar o arquivo de erro para tomar providências.
		MSG="No while lendo $linha do arquivo: ${lc_corrigir_erro_trf}"
		while read linha
		do
			Debug 1 "$MSG [Função:$FUNCNAME Linha:$LINENO]"
# Salva o arquivo de erro com 1 item a menos porque estamos
# tomando providências. 
			grep -v $linha ${ARQ_TAREFA} > $lc_tmp_tarefa
# Move o arquivo de tarefa encontrado nas definições de erro. 
			mv  $lc_tmp_tarefa ${ARQ_TAREFA}
# Definindo o caminho e nome do arquivo de da tarefa.
# Se existe, vou apagar porque antes deu erro.
		MSG="${dir_pasta_start}/Erro_${$}_Start_Service"
		lc_arq_erro_trf="$MSG_${ORIGEM}_canal_${linha}.trf"

			if [ -f ${lc_arq_erro_trf} ]
			then
				rm ${lc_arq_erro_trf}
			fi
		done < "${lc_corrigir_erro_trf}" 
		# Arquivo que contém os erros.

# Finalmente, após ter verificado o arquivo de erro,
# vou apaga-lo para não fica sujeita no HD.
		if [ -f ${lc_corrigir_erro_trf} ]
		then
			rm ${lc_corrigir_erro_trf}
		fi
         else
            StatusErroTarefa="0"
	fi
	# Agora a lista está limpa para disparar os scripts.
        echo $StatusErroTarefa 
}
 
